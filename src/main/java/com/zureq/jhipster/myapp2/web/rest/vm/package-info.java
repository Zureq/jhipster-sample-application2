/**
 * View Models used by Spring MVC REST controllers.
 */
package com.zureq.jhipster.myapp2.web.rest.vm;
